class Api::AbilitiesController < ApplicationController
	def index
		render json: Overwatch.new.abilities
	end

	def show
		render json: Overwatch.new.ability(params[:id])
	end
end