class Api::HerosController < ApplicationController
	def index
		render json: Overwatch.new.heros
  end

  def show
  	render json: overwatch_fetch
  end

  def abilities
  	render json: overwatch_fetch['abilities']
  end

  private

  def overwatch_fetch
  	Overwatch.new.send(:hero, params[:id])
  end
end