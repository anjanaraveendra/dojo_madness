class Overwatch
  include HTTParty
  BASE_URI = 'overwatch-api.net'
  base_uri BASE_URI

  HERO_ATTRIBUTES = ['id', 'name', 'real_name', 'health', 'armour', 'shield', 'abilities'].freeze
  ABILITY_ATTRIBUTES = ['id', 'name', 'description', 'is_ultimate'].freeze

  def initialize
    @options = { query: { site: BASE_URI } }
  end

  def heros
    fetch_data('/api/v1/hero')['data']
  end

  def hero(hero_id)
    fetch_data("/api/v1/hero/#{hero_id}").slice(*HERO_ATTRIBUTES)
  end

  def abilities
    fetch_data('/api/v1/ability')['data']
  end

  def ability(ability_id)
    fetch_data("/api/v1/ability/#{ability_id}").slice(*ABILITY_ATTRIBUTES)
  end

  private

  def fetch_data(url)
    self.class.get(url, @options).parsed_response
  end
end